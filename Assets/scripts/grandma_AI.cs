﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class grandma_AI : MonoBehaviour
{

    private bool attacking = false;
    public int health = 100;

    public Collider2D rollingPinTrigger;
    private Animator anim;

    public float nextFire = -1.0f;

    private bool throwAttacking;
    public float moveSpeed;
    public GameObject projectile;
    public float projectileSpeed = 5;
    public GameObject grandmaObject;
    private Rigidbody2D bulletRigid;

    private void Start()
    {

        nextFire = Time.time;
    }

    private void Awake()
    {
        rollingPinTrigger.enabled = false;
        anim = gameObject.GetComponent<Animator>();
    }

    private void Update()
    {
        GameObject farColl = GameObject.Find("farCollider");
        farCollider farColliderRef = farColl.GetComponent<farCollider>();

        GameObject closeColl = GameObject.Find("closeCollider");
        closeCollider closeColliderRef = closeColl.GetComponent<closeCollider>();

        if (!attacking && closeColliderRef.closeTrigger == true)
        {
            rollingPinTrigger.enabled = true;
            anim.SetBool("rollingPinAttack", true);
        }
        else if (closeColliderRef.closeTrigger == false) {
            rollingPinTrigger.enabled = false;
            anim.SetBool("rollingPinAttack", false);
        }

        if (!attacking && farColliderRef.farTrigger == true)
        {
            anim.SetBool("throwAttack", true);
            throwAttacking = true;
        }
        else if (farColliderRef.farTrigger == false)
        {
            anim.SetBool("throwAttack", false);
            throwAttacking = false;
        }

        //GRANDMA THROW
        if (Time.time >= nextFire)
        {
            if (throwAttacking == true)
            {
                nextFire = nextFire + 1;
                Fire();
            }
        }

        if (health == 0) {
            Destroy(this);
        }
        



        //if (!attacking && health == 100 && farColliderRef.farTrigger == true)
        //{
        //    rollingPinTrigger.enabled = true;
        //    anim.SetBool("rollingPinAttack", true);
        //}

    }

    private void Fire() {
        GameObject bullet = Instantiate(projectile, transform.position + new Vector3(0, -.5f, 0), Quaternion.identity) as GameObject;
        bulletRigid = bullet.GetComponent<Rigidbody2D>();
        bulletRigid.velocity = new Vector3(0f, -projectileSpeed);

    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.collider.CompareTag("playerProjectile"))
        {
            health -= 10;

        }
    }

}
