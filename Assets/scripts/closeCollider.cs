﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class closeCollider : MonoBehaviour {

    public bool closeTrigger = false;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {


	}

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "player")
        {
            closeTrigger = true;
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.tag == "player")
        {
            closeTrigger = false;
        }
    }

}
