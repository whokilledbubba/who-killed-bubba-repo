﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DialogueBox : MonoBehaviour
{
    [SerializeField] private GameObject root;
    [SerializeField] private Text textArea;
    private DialogueAsset currentDialogue;
    private int currentItem;
    private bool active;
    private bool acceptInput = true;

    private int currentChar;
    private bool textDone;
    private string currentText;
    private IEnumerator displayRoutine;
    [SerializeField] private float characterDelay = 0.2f;
    private float characterDelayScale = 1;

    private void Awake()
    {
        Hide();
    }

    public void Display(DialogueAsset newDialogue)
    {
        currentDialogue = newDialogue;
        Display();
    }
    public void Display()
    {
        root.SetActive(true);
        ResetItem();
        if (currentDialogue) NextItem();
        active = true;
    }
    public void Hide()
    {
        textArea.text = "";
        root.SetActive(false);
        active = false;
    }
    public void ResetItem()
    {
        currentItem = 0;
    }

    public void ProcessItem(DialogueItem d)
    {
        acceptInput = d.timeout <= 0;
        if (!acceptInput)
        {
            float delay = d.timeout;
            if (d.waitForText) delay += d.text.Length * characterDelay * d.characterDelayScale;
            Invoke("NextItem", delay);
        }

        if (d.isFunctionCall) SendMessage(d.text, SendMessageOptions.DontRequireReceiver);
        else
        {
            textDone = !d.waitForText;
            characterDelayScale = d.characterDelayScale;
            SetText(d.text);
        }
    }
    public void NextItem()
    {
        DialogueItem d = currentDialogue.Next(currentItem, out currentItem);
        if (d == null) Hide();
        else ProcessItem(d);
    }
    private void SetText(string t)
    {
        currentText = t;
        if (displayRoutine != null) StopCoroutine(displayRoutine);
        displayRoutine = DisplayText();
        StartCoroutine(displayRoutine);
    }

    public void SetDialogue(DialogueAsset newDialogue)
    {
        currentDialogue = newDialogue;
        currentItem = 0;
    }

    private IEnumerator DisplayText()
    {
        textArea.text = "";
        for (int i = 0; i < currentText.Length; i++)
        {
            textArea.text = currentText.Substring(0, i + 1);
            yield return new WaitForSecondsRealtime(characterDelay * characterDelayScale);
        }
        textDone = true;
        yield return null;
    }

    private void Update()
    {
        if (active && acceptInput && Input.GetKeyDown(KeyCode.F) && textDone) NextItem();
    }
}
