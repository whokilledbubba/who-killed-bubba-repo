﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DialogueActivator : MonoBehaviour {
    public DialogueAsset TextAssest;

    public DialogueBox box;

    //Start Dialgue
    void OnTriggerEnter2D(Collider2D collision)
    {
        box.Display(TextAssest);
    }
}
