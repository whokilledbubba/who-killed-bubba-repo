﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Dialogue", menuName = "Dialogue Asset", order = 0)]
public class DialogueAsset : ScriptableObject
{
    [SerializeField]
    private DialogueItem[] items = new DialogueItem[1];
    public int Length { get { return items.Length; } }

    public DialogueItem Get(int index)
    {
        if (index >= 0 && index < Length)
            return items[index];
        else
            return null;
    }
    public DialogueItem Next(int inIndex, out int outIndex)
    {
        outIndex = inIndex + 1;
        return (Get(inIndex));
    }
}

[System.Serializable]
public class DialogueItem
{
    [TextArea] public string text;
    public float timeout = 0; // timeout <= 0 is ignored
    public bool waitForText = false;
    public float characterDelayScale = 1;
    public bool isFunctionCall;
}
